'use strict';

module.exports = {
	root: true,
	extends: ['bluedrop', 'bluedrop/config/node', 'bluedrop/config/ecmascript-8'],
	parserOptions: {
		ecmaVersion: 2018,
	},
};
