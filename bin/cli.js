#!/usr/bin/env node
'use strict';

const program = require('commander');
const {readdirSync, readFileSync} = require('fs');
const {YamlPrinter} = require('../src/printers');
const blueprintFunctions = require('../src/helpers/functions');

program
	.command('render')
	.description('render a yaml service configuration file based on a template')
	.version('1.0.0')
	.option('-c --config <config>', 'Configuration file that contains enironment specific variables')
	.option('-d --directory <directory>', 'Directory of templates that will be used for generating yaml files')
	.option('-o --output <output>', 'Output directory')
	.action((opts) => {
		const {config, directory, output} = opts;
		if (!config || !directory || !output) {
			return process.stderr.write('Required options not defined');
		}

		let providedConfig;
		let blueprintVariables;
		let options;
		try {
			providedConfig = JSON.parse(readFileSync(config));
			blueprintVariables = providedConfig.variables;
			options = providedConfig.options;
		}
		catch (e) {
			return process.stderr.write(`Could not parse config file: ${e.message}`);
		}

		let blueprints;
		try {
			blueprints = readdirSync(directory);
		}
		catch (e) {
			return process.stderr.write(`Could not read blueprints directory: ${e.message}`);
		}
		try {
			return blueprints.forEach((blueprint) => new YamlPrinter({
				blueprintPath: `${directory}/${blueprint}`,
				outputPath: `${output}/${blueprint.replace('.template', '')}`,
				options,
				blueprintVariables,
				blueprintFunctions,
			}).print()
			);
		}
		catch (e) {
			return process.stderr.write(`Error printing files from blueprints: ${e.message}`);
		}
	});

program.parse(process.argv);
