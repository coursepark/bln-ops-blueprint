'use strict';
/* eslint-disable promise/prefer-await-to-callbacks */

const awaitFilter = async (fnPromise, cb) => {
	try {
		const result = await fnPromise;
		return cb(null, result);
	}
	catch (e) {
		return cb(e);
	}
};

module.exports = awaitFilter;
