'use strict';

const yaml = require('./yaml');

module.exports = {
	YamlPrinter: yaml,
};
