'use strict';

const nunjucks = require('nunjucks');
const {writeFile} = require('fs');
const {promisify} = require('util');

const awaitFilter = require('../filters/await');

const VARIABLE_START_PATTERN = '<$';
const VARIABLE_END_PATTERN = '$>';

class YamlPrinter {
	constructor(options = {}) {
		this.blueprintPath = options.blueprintPath;
		this.outputPath = options.outputPath;
		this.blueprintVariables = options.blueprintVariables;
		this.blueprintFunctions = options.blueprintFunctions;
		this.options = options.options;

		const environment = nunjucks.configure({
			...this.options,
			tags: {
				blockStart: '<%',
				blockEnd: '%>',
				variableStart: VARIABLE_START_PATTERN,
				variableEnd: VARIABLE_END_PATTERN,
				commentStart: '<#',
				commentEnd: '#>',
			},
		});
		environment.addFilter('await', awaitFilter, true);
	}

	async print() {
		const {render} = nunjucks;
		const renderedTemplate = await promisify(render)(this.blueprintPath, {
			...this.blueprintVariables,
			...this.blueprintFunctions,
		});
		return promisify(writeFile)(this.outputPath, renderedTemplate);
	}
}

module.exports = YamlPrinter;
