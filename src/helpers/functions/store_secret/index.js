'use strict';

const {Squrl} = require('bln-squrl');

// eslint-disable-next-line no-process-env
const SQURL_VAULT_NAME = process.env.SQURL_VAULT_NAME || 'kube-dev';

const storeSecret = async (value) => {
	const squrl = new Squrl({store: 'aws', vault: SQURL_VAULT_NAME});
	let secretKey;
	try {
		secretKey = await squrl.create({value});
	}
	catch (e) {
		if (e.message.includes('already exists')) {
			return e.message.split('secret ')[1].split(' ')[0];
		}
		process.stdout.write(e.message);
	}
	return secretKey;
};

module.exports = storeSecret;
