'use strict';

const store = {};
const semaphore = {};
const AsyncFunction = (async () => {}).constructor;

const parseKeyStoreArgs = (args) => {
	const key = args[0];
	const fn = args[1];
	const fnArgs = args.slice(2);
	return {key, fn, fnArgs};
};

const keyStore = (...args) => {
	if (args[1] instanceof AsyncFunction) {
		return keyStore.async(...args);
	}
	const {key, fn, fnArgs} = parseKeyStoreArgs(args);
	if (!store[key]) {
		if (typeof fn === 'function') {
			store[key] = fn(...fnArgs);
		}
		else {
			store[key] = fn;
		}
	}

	return store[key];
};

keyStore.async = async (...args) => {
	const {key, fn, fnArgs} = parseKeyStoreArgs(args);
	if (semaphore[key]) {
		await semaphore[key];
	}
	if (!store[key]) {
		semaphore[key] = Promise.resolve(fn(...fnArgs));
		store[key] = await semaphore[key];
	}
	return store[key];
};

module.exports = keyStore;
