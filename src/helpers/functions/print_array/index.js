'use strict';

const printArray = (array) => JSON.stringify(array);

module.exports = printArray;
