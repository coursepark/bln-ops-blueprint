# bln-ops-blueprint

## Description

A NodeJS CLI tool for rendering templates based on provided configuration. Useful when generating K8s YAML configuration files for new environments or clusters.

## Requirements

-   NodeJS 8.10 or higher
-   NPM 6.10 or higher

Run `npm i` to install required dependencies and `npm link` to add the CLI tool to

## Usage

To see available commands:

```
$ blueprint -h
Usage: blueprint [options] [command]

Options:
  -h, --help        output usage information

Commands:
  render [options]  render a yaml service configuration file based on a template
```

### Render templates

`render` command requires a config file (a sample config file is given in `config/defaults.json`), a directory containing template (`templates/example.yaml.template`), and an output directory:

```
blueprint render -h
Usage: render [options]

render a yaml service configuration file based on a template

Options:
  -V, --version               output the version number
  -c --config <config>        Configuration file that contains enironment specific variables
  -d --directory <directory>  Directory of templates that will be used for generating yaml files
  -o --output <output>        Output directory
  -h, --help                  output usage informatio
```

Example:
`blueprint render -c config/defaults.json -d templates -o ./`
